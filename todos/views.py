from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
def todo_list_list(request):
    all_todo_lists = TodoList.objects.all()
    context = {
        "all_todo_lists": all_todo_lists,
    }
    return render(request, "todos/list.html" , context)

def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo_list,
    }

    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            new_todo_list = form.save(False)
            new_todo_list.save()
            return redirect("todo_list_detail", id=new_todo_list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    edited_list = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        form = TodoListForm(request.POST, instance=edited_list)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=edited_list.id)
    else:
        form = TodoListForm(instance=edited_list)

    context = {
        "edited_list": edited_list,
        "form": form,
    }

    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid:
            todo_item = form.save(False)
            todo_item.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }

    return render(request, "todos/createchild.html", context)

def todo_item_update(request, id):
    updated_item = get_object_or_404(TodoItem, id=id)

    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=updated_item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=updated_item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }

    return render(request, "todos/editchild.html", context)
